package com.example.q21;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q21.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText etext;
    TextView text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        etext= findViewById(R.id.etext);
        text2 = findViewById(R.id.text2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grade = etext.getText().toString();

                switch (grade)
                {
                    case "A":
                        text2.setText("Excellent");
                        break;
                    case "B":
                        text2.setText("Very good");
                        break;
                    case "C":
                        text2.setText("Good");
                        break;
                    case "D":
                        text2.setText("Passed");
                        break;
                    case "E":
                        text2.setText("Fair");
                        break;
                    case "F":
                        text2.setText("Failed");
                        break;
                    default:
                        text2.setText("Invalid grade");
                        break;

                }
            }
        });

    }
}